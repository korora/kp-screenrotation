%define debug_package %{nil}
%global commit 1a78a4b180db517a044958156320966f944aa75a
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:		screenrotator	
Version:	0.1.0
Release:	0.git%{shortcommit}%{?dist}
Summary:	Automatic screen rotation daemon for X11

Group:		User Interface/X Hardware Support
License:	GPL3
URL:		https://github.com/GuLinux/ScreenRotator
Source0:	https://github.com/GuLinux/ScreenRotator/archive/%{commit}.tar.gz#/ScreenRotator-%{commit}.tar.gz

BuildRequires:  gcc, gcc-c++
BuildRequires:	libXi-devel, libXrandr-devel
BuildRequires:	cmake, qt5-qttools, qt5-qtbase-devel,qt5-qtx11extras-devel, qt5-qtsensors-devel
Requires:	qt5-qtx11extras, qt5-qtbase, qt5-qtsensors, libXi, libXrandr, iio-sensor-proxy

%description
Simple Qt screen rotation manager reads from accelerometer sensors, and rotates the display according to the readings. Currently Simple Qt screen rotation manager only on X11. Simple Qt screen rotation manager is similar to the current solution implemented in Gnome, but works on all other desktop environments as well (KDE, XFCE, etc).

%prep
%setup -n ScreenRotator-%{commit}

%build
mkdir -p build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release .. 

%make_build

%install
cd build
%make_install


%files
%{_bindir}/screenrotator
%{_datadir}/applications/screenrotator.desktop
%{_datadir}/icons/hicolor/
%{_sysconfdir}/xdg/autostart/screenrotator-autostart.desktop

%changelog
* Tue Sep 04 2018 JMiahMan <JMiahMan@unity-linux.org> - 0.1.0-0.git1a78a4b
- Initial Build
